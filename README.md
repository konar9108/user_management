# README #

Worker and Department manager using MySQL DB.
Program created for training purposes by Damian Konarowski.

### What is this repository for? ###

* Two entities: department and worker combined together with a relation using hibernate.
* User can do all CRUD operations using CLI (Command Line Interface).

### How do I get set up? ###

* persistence.xml configured with db.
* dependecies imported with Maven: jaxb-api, mysql-connector-java, hibernate-entitymanager, lombok

### Contribution guidelines ###
Created by Konar9108.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact