package pl.sda.jpa.dao;

import pl.sda.jpa.model.Department;
import pl.sda.jpa.util.JPAUtil;

import javax.persistence.Query;
import java.util.List;

import static pl.sda.jpa.dao.DaoUtil.getdeptDao;
import static pl.sda.jpa.util.cli.InputHelper.validateIntInput;


public class DepartmentDao implements GenericDao<Department> {
    @Override
    public Department get(long id) {
       Department dept = JPAUtil.getEntityManager().find(Department.class, id);
        if (dept == null){
            System.out.println("Department not found.");
        }
            return dept;
    }

    @Override
    public void delete(long id) {

            Department dept = get(id);
            if (dept != null) {
                JPAUtil.doInTransaction((entityManager) -> entityManager.remove(dept));
            } else {
                System.out.println("Department not found");
            }
    }

    @Override
    public List<Department> getAll() {
        Query query = JPAUtil.getEntityManager().createNamedQuery("getAllDepartments", Department.class);
        return query.getResultList();
    }

    @Override
    public void update(Department entity) {
        JPAUtil.doInTransaction((entityManager -> {
            entityManager.merge(entity);
        }));
    }

    @Override
    public Department save(Department entity) {
        JPAUtil.doInTransaction((entityManager) -> {
            entityManager.persist(entity);
        });
        return entity;
    }

    public Department findByName(String name) {
        Query query = JPAUtil.getEntityManager().createQuery("from Department d where name like :name");
        query.setParameter("name", "%" + name + "%");
        return (Department) query.getSingleResult();
    }

    public Department findDeptLoopInput() {
        Department department = null;
        while (department == null) {
            System.out.println("Type id to find department. Type 0 to exit.");
            int opt = validateIntInput();
            if (opt == 0) return null;
            department = getdeptDao().get(opt);
            if ( department == null) {
                System.out.println("Department not found.");
            }
        }
        return department;
    }

}

