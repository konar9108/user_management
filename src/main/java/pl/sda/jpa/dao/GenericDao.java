package pl.sda.jpa.dao;

import java.util.List;

/**
 * Generic Interface for DAO classes making CRUD operations.
 * @param <T>
 */
public interface GenericDao<T> {

    /**
     * return Entity from given Id
     * @param id entity identifier
     * @return entity
     */
    T get(long id);

    /**
     * delete Entity from given Id
     * @param id entity identifier
     */
    void delete(long id);

    /**
     * return list of all records in entity
     * @return all entities
     */
    List<T> getAll();

    /**
     *  adds given entity to database
     * @param entity
     * @return entity
     */
    T save(T entity);


    /**
     * modifies given entity
     * @param entity
     */
    void update(T entity);


}
