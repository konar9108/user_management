package pl.sda.jpa.dao;

import pl.sda.jpa.model.Department;
import pl.sda.jpa.model.WorkerBuilder;

import java.time.LocalDateTime;

public class DaoFactory {

    private static DepartmentDao departmentDao;
    private static WorkerDao workerDao;

    private DaoFactory() {
        // NOP
    }

    public static GenericDao getInstance (Class clazz) {
        if (clazz.equals(DepartmentDao.class)){
            if (departmentDao == null){
               departmentDao = new DepartmentDao();
            }
            return departmentDao;
        }
        if (clazz.equals(WorkerDao.class)){
            if (workerDao == null){
                workerDao = new WorkerDao();
            }
            return workerDao;
        }
        throw new IllegalArgumentException();
    }

    public static void setupDP() {
        getInstance(DepartmentDao.class).save(new Department("IT"));
        getInstance(DepartmentDao.class).save(new Department("Administration"));
        getInstance(DepartmentDao.class).save(new Department("HR"));

        getInstance(WorkerDao.class).save(new WorkerBuilder().setFirst_name("Jan").setLast_name("Janowicz").setAge(55).setHire_date(LocalDateTime.now()).setDepartment((Department) getInstance(DepartmentDao.class).get(1)).build());
        getInstance(WorkerDao.class).save(new WorkerBuilder().setFirst_name("Damian").setLast_name("Konar").setAge(29).setHire_date(LocalDateTime.now()).setDepartment((Department) getInstance(DepartmentDao.class).get(1)).build());
        getInstance(WorkerDao.class).save(new WorkerBuilder().setFirst_name("Bogdan").setLast_name("Bogdański").setAge(39).setHire_date(LocalDateTime.now()).setDepartment((Department) getInstance(DepartmentDao.class).get(2)).build());
        getInstance(WorkerDao.class).save(new WorkerBuilder().setFirst_name("Adam").setLast_name("Adamowicz").setAge(62).setHire_date(LocalDateTime.now()).setDepartment((Department) getInstance(DepartmentDao.class).get(3)).build());
    }

}
