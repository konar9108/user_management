package pl.sda.jpa.dao;

import pl.sda.jpa.model.Department;
import pl.sda.jpa.model.WorkerBuilder;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;

public class DaoUtil {

    private DaoUtil() {
    }

    private static DepartmentDao deptDao;
    private static WorkerDao workerDao;

    public static DepartmentDao getdeptDao() {
        if (deptDao == null) {
            deptDao = new DepartmentDao();
        }
        return deptDao;
    }

    public static WorkerDao getworkerDao() {
        if (workerDao == null) {
            workerDao = new WorkerDao();
        }
        return workerDao;
    }

    public static void setupDP() {
        getdeptDao().save(new Department("IT"));
        getdeptDao().save(new Department("Administration"));
        getdeptDao().save(new Department("HR"));

        getworkerDao().save(new WorkerBuilder().setFirst_name("Jan").setLast_name("Janowicz").setAge(55).setHire_date(LocalDateTime.now()).setDepartment(deptDao.get(1)).build());
        getworkerDao().save(new WorkerBuilder().setFirst_name("Damian").setLast_name("Konar").setAge(29).setHire_date(LocalDateTime.now()).setDepartment(deptDao.get(1)).build());
        getworkerDao().save(new WorkerBuilder().setFirst_name("Bogdan").setLast_name("Bogdański").setAge(39).setHire_date(LocalDateTime.now()).setDepartment(deptDao.get(2)).build());
        getworkerDao().save(new WorkerBuilder().setFirst_name("Adam").setLast_name("Adamowicz").setAge(62).setHire_date(LocalDateTime.now()).setDepartment(deptDao.get(3)).build());
    }

}
