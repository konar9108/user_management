package pl.sda.jpa.dao;

import pl.sda.jpa.model.Worker;
import pl.sda.jpa.util.JPAUtil;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class WorkerDao implements GenericDao<Worker> {
    @Override
    public Worker get(long id) {
        Worker worker = JPAUtil.getEntityManager().find(Worker.class,id);
        if (worker == null){
            System.out.println("Worker not found.");
        }
            return worker;
    }

    @Override
    public void delete(long id) {
            Worker worker = get(id);
            if (worker != null) {
        JPAUtil.doInTransaction((entityManager) -> entityManager.remove(worker));
    }
    }

    @Override
    public List<Worker> getAll() {
        Query query = JPAUtil.getEntityManager().createQuery("from Worker w", Worker.class);

        if (query.getResultList().isEmpty()){
            System.out.println("There are no workers.");
            return new ArrayList<>();
        }
        return query.getResultList();

    }

    @Override
    public void update(Worker entity) {
        JPAUtil.doInTransaction((entityManager -> {
            entityManager.merge(entity);
        }));
    }

    @Override
    public Worker save(Worker entity) {
        JPAUtil.doInTransaction((entityManager) -> {
            entityManager.persist(entity);
        });
        return entity;
    }
}
