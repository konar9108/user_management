package pl.sda.jpa.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class Worker {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long worker_id;

    @Column(length = 50, nullable = false)
    private String first_name;

    @Column(length = 50, nullable = false)
    private String last_name;

    @Column(nullable = false)
    private int age;


    private LocalDateTime hire_date;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "department_id")
    private Department department;

    public void setDepartment(Department department) {
        this.department = department;
        department.getWorkerList().add(this);
    }

    public Worker( String first_name, String last_name, int age, LocalDateTime hire_date, Department department) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.age = age;
        this.hire_date = hire_date;
        this.department = department;
        department.getWorkerList().add(this);
    }

    @Override
    public String toString() {
        return "Worker{" +
                "worker_id=" + worker_id +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", age=" + age +
                ", hire_date=" + hire_date.withNano(0).withSecond(0) +
                ", department=" + department +
                '}';
    }
}
