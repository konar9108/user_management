package pl.sda.jpa.model;

import java.time.LocalDateTime;

public class WorkerBuilder {
    private String first_name;
    private String last_name;
    private int age;
    private LocalDateTime hire_date;
    private Department department;

    public WorkerBuilder setFirst_name(String first_name) {
        this.first_name = first_name;
        return this;
    }

    public WorkerBuilder setLast_name(String last_name) {
        this.last_name = last_name;
        return this;
    }

    public WorkerBuilder setAge(int age) {
        this.age = age;
        return this;
    }

    public WorkerBuilder setHire_date(LocalDateTime hire_date) {
        this.hire_date = hire_date;
        return this;
    }

    public WorkerBuilder setDepartment(Department department) {
        this.department = department;
        return this;
    }

    public Worker build() {
        return new Worker(first_name, last_name, age, hire_date, department);
    }
}