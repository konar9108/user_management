package pl.sda.jpa.util.cli.menu.worker;

import pl.sda.jpa.util.cli.Page;

import static pl.sda.jpa.dao.DaoUtil.*;
import static pl.sda.jpa.util.cli.InputHelper.validateIntInput;

public class DeleteWorkerPage implements Page {
    @Override
    public void doAction() {
        getworkerDao().getAll().forEach(System.out::println);
        System.out.println("Type Worker Id: ");
        getworkerDao().delete(validateIntInput());

    }
}
