package pl.sda.jpa.util.cli.menu.department;

import pl.sda.jpa.util.cli.Page;
import static pl.sda.jpa.dao.DaoUtil.getdeptDao;

public class PrintDepartmentsPage implements Page {

    @Override
    public void doAction() {
   getdeptDao().getAll().forEach(System.out::println);
    }
}


