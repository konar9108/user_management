package pl.sda.jpa.util.cli.menu;
import lombok.Getter;
import pl.sda.jpa.util.cli.Page;

@Getter
public class MenuItem {
    private final String text;

    private final int option;

    private final Page parent;

    private final Page child;

    public MenuItem(final String text, final int option,final Page parent,final Page child) {
        this.text = text;
        this.option = option;
        this.parent = parent;
        this.child = child;
    }
}
