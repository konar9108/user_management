package pl.sda.jpa.util.cli.menu.worker;

import pl.sda.jpa.model.Worker;
import pl.sda.jpa.util.cli.Page;
import static pl.sda.jpa.util.cli.InputHelper.*;
import static pl.sda.jpa.dao.DaoUtil.*;

public class FindWorkerPage implements Page {
    @Override
    public void doAction() {
            System.out.println("Type worker Id:");
            System.out.println(getworkerDao().get(validateIntInput()));
    }
}
