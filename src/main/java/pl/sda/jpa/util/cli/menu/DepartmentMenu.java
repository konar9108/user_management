package pl.sda.jpa.util.cli.menu;
import pl.sda.jpa.util.cli.Page;
import pl.sda.jpa.util.cli.menu.department.*;

import java.util.Arrays;
import java.util.List;

public class DepartmentMenu extends AbstractMenu {

    public DepartmentMenu(Page mainMenu) {
        this.previousMenu = mainMenu;
    }
    private final Page previousMenu;

    @Override
    List<MenuItem> getItems() {
        return Arrays.asList(
                new MenuItem("Add", 1, previousMenu, new AddDepartmentPage()),
                new MenuItem("Delete", 2, previousMenu, new DeleteDepartmentPage()),
                new MenuItem("Update", 3, previousMenu, new UpdateDepartmentPage()),
                new MenuItem("Print workers in given department", 4, previousMenu, new PrintWorkersInDeptPage()),
                new MenuItem("Print departments", 5, previousMenu, new PrintDepartmentsPage()),
                new MenuItem("Go back", 0, previousMenu, null));
    }
    }

