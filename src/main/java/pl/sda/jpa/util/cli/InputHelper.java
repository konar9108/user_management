package pl.sda.jpa.util.cli;

import java.util.InputMismatchException;
import java.util.Scanner;

public class InputHelper {

    private InputHelper() {
    }

    private static Scanner scn;

    public static Scanner getScn() {
        if (scn == null) {
            scn = new Scanner(System.in);
        }
        return scn;
    }

    public static int validateIntInput () {
        System.out.print("Type Integer: ");
        int validInt;
        while(true){
        try {
            validInt = getScn().nextInt();
            getScn().nextLine();
            return validInt;
        } catch (InputMismatchException ime) {
            System.out.println("Invalid Input. Try again.");
            }
        }
    }
}
