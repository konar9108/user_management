package pl.sda.jpa.util.cli.menu;
import pl.sda.jpa.util.cli.InputHelper;
import pl.sda.jpa.util.cli.Page;

import java.util.Arrays;
import java.util.List;


public class MainMenu extends AbstractMenu implements Page {

    @Override
    List<MenuItem> getItems() {
        return Arrays.asList(
                new MenuItem("Departments", 1, null, new DepartmentMenu(this)),
                new MenuItem("Workers", 2, null, new WorkerMenu(this)),
                new MenuItem("Exit", 0, null, null));
    }

}


