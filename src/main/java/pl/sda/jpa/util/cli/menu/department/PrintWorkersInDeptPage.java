package pl.sda.jpa.util.cli.menu.department;

import pl.sda.jpa.model.Department;
import pl.sda.jpa.util.cli.Page;


import static pl.sda.jpa.dao.DaoUtil.getdeptDao;

public class PrintWorkersInDeptPage implements Page {

    @Override
    public void doAction() {
   getdeptDao().getAll().forEach(System.out::println);
   Department department = getdeptDao().findDeptLoopInput();
   if (department != null)
   department.getWorkerList().forEach(System.out::println);

    }
}


