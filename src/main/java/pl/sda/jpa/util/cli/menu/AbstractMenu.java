package pl.sda.jpa.util.cli.menu;

import pl.sda.jpa.util.cli.InputHelper;
import pl.sda.jpa.util.cli.Page;

import java.util.List;


public abstract class AbstractMenu implements Page {


    abstract List<MenuItem> getItems();

    @Override
    public void doAction() {
        while (true) {
            getItems().forEach((item) -> {
                System.out.println(item.getOption() + ". " + item.getText());
            });
            int choice = InputHelper.validateIntInput();
            MenuItem found = getItems().stream().filter((op) -> op.getOption() == choice).findFirst().orElse(null);

            if (found != null) {
                if (found.getOption() == 0 && found.getParent() == null) {
                    System.out.println("Dziękujemy za skorzystanie z tego fantastycznego programu.");
                    System.exit(1);
                } else if (found.getOption() == 0 && found.getParent() != null) {
                    found.getParent().doAction();
                } else {
                    found.getChild().doAction();
                }
            }
        }
    }
}
