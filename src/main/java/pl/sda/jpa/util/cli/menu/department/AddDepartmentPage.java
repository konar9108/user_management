package pl.sda.jpa.util.cli.menu.department;

import pl.sda.jpa.model.Department;
import pl.sda.jpa.util.cli.Page;

import static pl.sda.jpa.dao.DaoUtil.getdeptDao;
import static pl.sda.jpa.util.cli.InputHelper.getScn;

public class AddDepartmentPage implements Page {

    @Override
    public void doAction() {
        System.out.println("Write department name: ");
        String name = getScn().nextLine();
        System.out.println(name);
        getdeptDao().save(new Department(name));
        System.out.println("Department with name: " + name + " has been added");
    }
}


