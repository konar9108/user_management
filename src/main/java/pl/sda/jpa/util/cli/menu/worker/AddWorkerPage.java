package pl.sda.jpa.util.cli.menu.worker;

import pl.sda.jpa.model.Department;
import pl.sda.jpa.model.Worker;
import pl.sda.jpa.model.WorkerBuilder;
import pl.sda.jpa.util.Validators;
import pl.sda.jpa.util.cli.Page;

import java.time.LocalDateTime;

import static pl.sda.jpa.dao.DaoUtil.getdeptDao;
import static pl.sda.jpa.dao.DaoUtil.getworkerDao;
import static pl.sda.jpa.util.cli.InputHelper.getScn;
import static pl.sda.jpa.util.cli.InputHelper.validateIntInput;

public class AddWorkerPage implements Page {

    @Override
    public void doAction() {

        System.out.println("type first name of new worker: ");
        String firstName = getScn().nextLine();
        System.out.println("type last name of new worker: ");
        String lastName = getScn().nextLine();
        System.out.println("type age of new worker: ");
        int age = validateIntInput();
        getdeptDao().getAll().forEach(System.out::println);
        Department dept= null;
        do {
            System.out.println("type department Id to assign worker: ");
            int deptId = validateIntInput();
            dept = getdeptDao().get(deptId);
            if (dept == null){
                System.out.println("Department doesn't exist. Try again.");
            }
        } while(dept == null);
        System.out.println("Type correct hire-date in format (xxxx-xx-xxTxx:xx)");
        String str;
        do {
            str = getScn().nextLine();

        } while (!Validators.validateDatetime(str));
        LocalDateTime date = LocalDateTime.parse(str);
        Worker worker = new WorkerBuilder().setFirst_name(firstName)
                        .setLast_name(lastName).setAge(age)
                        .setDepartment(dept).setHire_date(date)
                        .build();
        getworkerDao().save(worker);
        System.out.println("Worker has been added. Specifics below: ");
        System.out.println(worker);

    }
}


