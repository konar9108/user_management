package pl.sda.jpa.util.cli.menu;

import pl.sda.jpa.util.cli.Page;
import pl.sda.jpa.util.cli.menu.worker.*;

import java.util.Arrays;
import java.util.List;

public class WorkerMenu extends AbstractMenu {

    public WorkerMenu(Page previousMenu) {
        this.previousMenu = previousMenu;
    }

    private final Page previousMenu;

    @Override
    List<MenuItem> getItems() {
        return Arrays.asList(
                new MenuItem("Add", 1, previousMenu, new AddWorkerPage()),
                new MenuItem("Delete", 2, previousMenu, new DeleteWorkerPage()),
                new MenuItem("Update", 3, previousMenu, new UpdateWorkerPage()),
                new MenuItem("Print workers", 4, previousMenu, new PrintWorkers()),
                new MenuItem("find worker by Id", 5, previousMenu, new FindWorkerPage()),
                new MenuItem("Go back", 0, previousMenu, null));
    }
    }

