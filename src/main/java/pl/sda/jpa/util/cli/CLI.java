package pl.sda.jpa.util.cli;

import pl.sda.jpa.dao.DepartmentDao;
import pl.sda.jpa.dao.WorkerDao;
import pl.sda.jpa.model.Department;
import pl.sda.jpa.model.Worker;
import pl.sda.jpa.model.WorkerBuilder;
import pl.sda.jpa.util.JPAUtil;

import java.time.LocalDateTime;
import java.util.Date;
import java.time.Instant;
import java.util.Scanner;

public class CLI {
    private static DepartmentDao deptDao;
    private static WorkerDao workerDao;

    // Dao and scanner getters
    /////////////////////
    public static DepartmentDao getdeptDao() {
        if (deptDao == null) {
            deptDao = new DepartmentDao();
        }
        return deptDao;
    }

    public static WorkerDao getworkerDao() {
        if (workerDao == null) {
            workerDao = new WorkerDao();
        }
        return workerDao;
    }

    public static Scanner getScn() {
        return new Scanner(System.in);
    }

    private static int getIntFromScanner() {
        boolean hasNextInt = getScn().hasNextInt();
        int option = -1;
        while (!hasNextInt) {
           if (!hasNextInt) {
               System.out.println("Invalid input! Try typing an int.");
           }
           getScn().nextInt();
            }
        return option;
    }
    //////////////////////////////
    // CLI methods

    public static void setupDP() {
        getdeptDao().save(new Department("IT"));
        getdeptDao().save(new Department("Administration"));
        getdeptDao().save(new Department("HR"));

        getworkerDao().save(new WorkerBuilder().setFirst_name("Jan").setLast_name("Janowicz").setAge(55).setHire_date(LocalDateTime.now()).setDepartment(deptDao.get(1)).build());
        getworkerDao().save(new WorkerBuilder().setFirst_name("Damian").setLast_name("Konar").setAge(29).setHire_date(LocalDateTime.now()).setDepartment(deptDao.get(1)).build());
        getworkerDao().save(new WorkerBuilder().setFirst_name("Bogdan").setLast_name("Bogdański").setAge(39).setHire_date(LocalDateTime.now()).setDepartment(deptDao.get(2)).build());
        getworkerDao().save(new WorkerBuilder().setFirst_name("Adam").setLast_name("Adamowicz").setAge(62).setHire_date(LocalDateTime.now()).setDepartment(deptDao.get(3)).build());
    }

    public void startCLI() {
        JPAUtil.getEntityManager().isOpen();
        setupDP();
        while (true) {
            System.out.println(
                    "\n choose Your option: " +
                            "\n 1. add" +
                            "\n 2. get" +
                            "\n 3. update" +
                            "\n 4. delete" +
                            "\n 5. exit");
            Scanner scanner = new Scanner(System.in);
            boolean hsnxt = scanner.hasNextInt();
            if(hsnxt)
            switch (scanner.nextInt()) {

                case 1:
                    addMenu();
                    continue;
                case 2:
                    getMenu();
                    continue;
                case 3:
                    updateMenu();
                    continue;
                case 4:
                    deleteMenu();
                    continue;
                case 5: {
                    System.out.println("Thank You for using this amazing program!");
                    return;
                }
                default:
                    System.out.println("Invalid input.");
            }
        }
    }

    private void addMenu() {
        while (true) {
            System.out.println("1. Add user " +
                    "\n2. Add department" +
                    "\n3. Return");


            switch (getIntFromScanner()) {
                case 1:
                    addUser();
                    continue;
                case 2:
                    addDepartment();
                    continue;
                case 3:
                    return;

                default:
                    System.out.println("Invalid Input");
            }
        }
    }

    private void addDepartment() {
        System.out.println("Write department name: ");
        String name = getScn().nextLine();
        System.out.println(name);
        getdeptDao().save(new Department(name));
        System.out.println("Department with name: " + name + " has been added");
    }

    private void addUser() {
        Worker worker = new WorkerBuilder().build();
        System.out.println("type first name of new worker: ");
        String firstName = getScn().nextLine();
        worker.setFirst_name(firstName);
        System.out.println("type last name of new worker: ");
        String lastName = getScn().nextLine();
        worker.setLast_name(lastName);
        System.out.println("type age of new worker: ");
        int age = getIntFromScanner();
        worker.setAge(age);
        getdeptDao().getAll().forEach(System.out::println);
        System.out.println("type department Id to assign worker: ");
        int deptId = getIntFromScanner();
        worker.setDepartment(getdeptDao().get(deptId));
        worker.setHire_date(LocalDateTime.now());
        getworkerDao().save(worker);
        System.out.println("Worker has been added. Specifics below: ");
        System.out.println(worker);
    }

    private void getMenu() {
        while (true) {
            System.out.println("1. Print all users " +
                    "\n2. Find User by ID" +
                    "\n3. Print all departments" +
                    "\n4. Find Department by ID" +
                    "\n5. Find Department by Name" +
                    "\n6. return");


            switch (getIntFromScanner()) {
                case 1:
                    getworkerDao().getAll().forEach(System.out::println);
                    continue;
                case 2:
                    findUserById();
                    continue;
                case 3:
                    getdeptDao().getAll().forEach(System.out::println);
                    continue;
                case 4:
                    findDepartmentById();
                    continue;
                case 5:
                    findDepartmentByName();
                    continue;
                case 6:
                    return;

                default:
                    System.out.println("Invalid input.");


            }
        }
    }

    private void findDepartmentByName() {
        System.out.println("Write department name:");
        String name = getScn().nextLine();
        System.out.println(deptDao.findByName(name));
    }

    private Department findDepartmentByName(String name) {
        Department department = deptDao.findByName(name);
        System.out.println(department);
        return department;
    }

    private void findDepartmentById() {
        System.out.println("Write department ID:");
        int id = getScn().nextInt();
        System.out.println(deptDao.get(id));
    }

    private Department findDepartmentById(int id) {
        Department department = getdeptDao().get(id);
        System.out.println(department);
        return department;
    }

    private Worker findUserById() {
        System.out.println("Type user Id:");
        int id = getIntFromScanner();
        Worker worker = getworkerDao().get(id);
        System.out.println(worker);
        return worker;
    }

    private Worker findUserById(int id) {
        Worker worker = getworkerDao().get(id);
        System.out.println(worker);
        return worker;
    }

    private void deleteMenu() {
        while (true) {
            System.out.println("1. delete worker by Id " +
                    "\n2. delete department by Id (all workers assigned will also be deleted)" +
                    "\n3. return");

            switch (getIntFromScanner()) {
                case 1:
                    System.out.println("write worker Id: ");
                    workerDao.delete(getIntFromScanner());
                    continue;
                case 2:
                    System.out.println("Type department Id: ");
                    deptDao.delete(getIntFromScanner());
                    continue;
                case 3:
                    return;

                default:
                    System.out.println("Invalid input.");
            }
        }
    }

    private void updateMenu() {
        System.out.println("1. Update User " +
                "\n2. Update Department" +
                "\n4. return");


        switch (getIntFromScanner()) {
            case 1:
                updateUser();
            case 2:
                updateDepartment();
            case 3:
                return;
            default:
                System.out.println("Invalid input");


        }
    }

    private void updateDepartment() {
        Department department = null;
        while (department == null) {
            System.out.println("Type name or a substring to find department for updating. Type 0 to exit.");
            String name = getScn().nextLine();
            if (name.equals("0")) return;
            department = findDepartmentByName(name);
            if ( department == null) {
                System.out.println("Department not found.");
            }
        }
        while(true){

            System.out.println("Choose option:");
            System.out.println("1. change department name" +
                    "\n2. commit changes." +
                    "\n3. return");

            switch (getIntFromScanner()) {

                case 1:
                    System.out.println("Write new department name");
                    department.setName(getScn().nextLine()); continue;
                case 2: getdeptDao().update(department);
                case 3: return;
                default: System.out.println("Invalid input");


            }
        }
    }

    private void updateUser() {
        Worker worker = null;
        while (worker == null) {
            System.out.println("Type id to find user for updating. Type 0 to exit.");
            int id = getIntFromScanner();
            if (id == 0) return;
            worker = findUserById(id);
            if ( worker == null) {
                System.out.println("User not found.");
            }
        }
        while(true){

        System.out.println("Choose option:");
        System.out.println("1. change first name" +
                "\n2. change last name" +
                "\n3. change age" +
                "\n4. change hire date" +
                "\n5. change department id" +
                "\n6. commit changes" +
                "\n7. return");

        switch (getIntFromScanner()) {


            case 1:
                System.out.println("Write new first name");
                worker.setFirst_name(getScn().nextLine()); continue;
            case 2:
                System.out.println("Write new last name");
                worker.setLast_name(getScn().nextLine()); continue;
            case 3:
                System.out.println("Write new age");
                worker.setAge(getIntFromScanner()); continue;
            case 4:
                System.out.println("not working yet."); continue;
            case 5:
                System.out.println("Write department id");
                worker.setDepartment(getdeptDao().get(getIntFromScanner())); continue;
            case 6:
                getworkerDao().update(worker); continue;
            case 7:
                return;
            default:
                System.out.println("Invalid input");

        }
        }
    }

}

