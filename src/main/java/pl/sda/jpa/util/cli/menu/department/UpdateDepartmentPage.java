package pl.sda.jpa.util.cli.menu.department;
import pl.sda.jpa.model.Department;
import pl.sda.jpa.util.cli.Page;

import static pl.sda.jpa.dao.DaoUtil.getdeptDao;
import static pl.sda.jpa.util.cli.InputHelper.*;

public class UpdateDepartmentPage implements Page {

    @Override
    public void doAction() {
        getdeptDao().getAll().forEach(System.out::println);
        Department department = getdeptDao().findDeptLoopInput();
        System.out.println(department);
        if (department == null) return;
        while(true){

            System.out.println("Choose option:");
            System.out.println("1. change department name" +
                    "\n2. commit changes." +
                    "\n0. return");

            switch (validateIntInput()) {

                case 1:
                    System.out.println("Write new department name");
                    department.setName(getScn().nextLine()); continue;
                case 2: getdeptDao().update(department);
                case 0: return;
                default: System.out.println("Invalid input");


            }
        }

    }


}


