package pl.sda.jpa.util.cli.menu.department;
import pl.sda.jpa.util.cli.Page;
import static pl.sda.jpa.util.cli.InputHelper.*;
import static pl.sda.jpa.dao.DaoUtil.*;

public class DeleteDepartmentPage implements Page {

    @Override
    public void doAction() {
        getdeptDao().getAll().forEach(System.out::println);
        System.out.println("Provide department Id: ");
        getdeptDao().delete(validateIntInput());

    }
}


