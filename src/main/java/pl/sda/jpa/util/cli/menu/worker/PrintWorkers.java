package pl.sda.jpa.util.cli.menu.worker;

import pl.sda.jpa.util.cli.Page;
import static pl.sda.jpa.dao.DaoUtil.*;

public class PrintWorkers implements Page {
    @Override
    public void doAction() {
        getworkerDao().getAll().forEach(System.out::println);
    }
}
