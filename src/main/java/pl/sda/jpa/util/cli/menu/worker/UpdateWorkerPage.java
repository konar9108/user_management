package pl.sda.jpa.util.cli.menu.worker;

import pl.sda.jpa.model.Worker;
import pl.sda.jpa.util.Validators;
import pl.sda.jpa.util.cli.Page;

import java.time.LocalDateTime;

import static pl.sda.jpa.dao.DaoUtil.*;
import static pl.sda.jpa.util.cli.InputHelper.*;

public class UpdateWorkerPage implements Page {
    @Override
    public void doAction() {
        Worker worker = null;
        while (worker == null) {
            System.out.println("Type id to find user for updating. Type 0 to exit.");
            int id = validateIntInput();
            if (id == 0) return;
            worker = getworkerDao().get(id);
            if (worker == null) {
                System.out.println("User not found.");
            }
        }
        while (true) {

            System.out.println("Choose option:");
            System.out.println("1. change first name" +
                    "\n2. change last name" +
                    "\n3. change age" +
                    "\n4. change hire date" +
                    "\n5. change department id" +
                    "\n6. commit changes" +
                    "\n7. return");

            switch (validateIntInput()) {


                case 1:
                    System.out.println("Write new first name");
                    worker.setFirst_name(getScn().nextLine());
                    continue;
                case 2:
                    System.out.println("Write new last name");
                    worker.setLast_name(getScn().nextLine());
                    continue;
                case 3:
                    System.out.println("Write new age");
                    worker.setAge(validateIntInput());
                    continue;
                case 4:
                    System.out.println("Type correct hire-date in format (yyyy-mm-ddThh:mm)");
                    String str;
                    do {
                        str = getScn().nextLine();
                        if (!Validators.validateDatetime(str)) {
                            System.out.println("Wrong format. Try Again.");
                        }
                    } while (!Validators.validateDatetime(str));
                    worker.setHire_date(LocalDateTime.parse(str));
                    System.out.println("Hire date changed.");
                    continue;
                case 5:
                    System.out.println("Write department id");
                    worker.setDepartment(getdeptDao().get(validateIntInput()));
                    continue;
                case 6:
                    getworkerDao().update(worker);
                    continue;
                case 7:
                    return;
                default:
                    System.out.println("Invalid input");

            }
        }
    }
}
