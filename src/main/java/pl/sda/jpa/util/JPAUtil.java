package pl.sda.jpa.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.function.Consumer;

public class JPAUtil {
    private static EntityManager entityManager;

    public static EntityManager getEntityManager() {

        if (entityManager == null) {
            EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("mysqlPU");
            entityManager = entityManagerFactory.createEntityManager();
        }
        return entityManager;
    }

    public static void doInTransaction (Consumer<EntityManager> action){
        EntityTransaction transaction = getEntityManager().getTransaction();
        try {
        transaction.begin();
            action.accept(getEntityManager());
            transaction.commit();
            System.out.println("Operation successful.");
        } catch (RuntimeException e){
            transaction.rollback();
            System.out.println("!!!Operation failed.!!!");
            e.printStackTrace();
        }




    }


}
