package pl.sda.jpa.util;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ValidatorsTest {

    private static Stream<Arguments> provideStringsForIsBlank() {
        return Stream.of(
                Arguments.of("1995-12-12T05:30:30", true),
                Arguments.of("1995-12-51R05:30:30", false),
                Arguments.of("19952-12-51T05:30:30", false),
                Arguments.of("1995-12-61T05:30:30", false),
                Arguments.of("1991-08-12T06:21:00", true)
        );
    }
    @ParameterizedTest
    @MethodSource("provideStringsForIsBlank")
    public void validateDate(String input, boolean expected) {
        assertEquals(expected, Validators.validateDatetime(input));
    }


//    @ParameterizedTest
//    @ValueSource(strings = {
//            "2020-01-01R12:00:00",
//            "2019-12-12T12:00:00",
//    })
//    void shouldFailValidateDateTimeTests(String str) {
//        //given
//        //when
//        boolean returned = Validators.validateDatetime(str);
//        //then
//        assertFalse(returned);
//    }

//    @ParameterizedTest
//    @ValueSource(strings = {
//            "2020-05-20T12:32:21",
//            "2019-12-12T03:21:54",
//            "1991-08-12T06:52:00",
//    })
//    void shouldPassValidateDateTimeTests(String str) {
//        //given
//        //when
//        boolean returned = Validators.validateDatetime(str);
//        //then
//        assertTrue(returned);
//    }
}